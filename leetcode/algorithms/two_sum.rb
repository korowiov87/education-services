def two_sum(nums, target)
  hash_map = Hash.new
  nums.each_with_index do |element, index| 
    hash_map[element] ||= index
    searched_element = target - element
    if hash_map[searched_element] && hash_map[searched_element] != index
      return [hash_map[searched_element], index]
    end
  end
end