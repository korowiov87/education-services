#include <cstdio>
#include <vector>
#include <cstdlib>
using namespace std;

struct P
{
  P() {}
  P(int _x, int _y) : x(_x), y(_y) {}
  int x;
  int y;
};

int main()
{
  vector <P> rect;
  int x,y,n,height, width, mheight = 0, mwidth = 0;
  bool ans = true;
  scanf("%d", &n);
  while(n--)
  {
    scanf("%d %d", &x, &y);
    rect.push_back(P(x,y));
    if(rect.size() > 1)
    {
      for(int i = rect.size() - 2; i >= 0; i--)
      {
        if(x != rect[i].x)
        {
          width = abs(x - rect[i].x);
          if(mwidth == 0) mwidth = width;
          else if(mwidth != width) ans = false;
        }
        if(y != rect[i].y)
        {
          height = abs(y - rect[i].y);
          if(mheight == 0) mheight = height;
          else if(mheight != height) ans = false;
        }
      }
    }
  }
  if(ans == true && rect.size() > 1 && mheight > 0 && mwidth > 0) printf("%d\n",(mheight * mwidth));
  else printf("-1\n");
  return 0;
}