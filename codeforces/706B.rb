n = gets.to_i
prices = gets.split.map(&:to_i).sort!
days = gets.to_i

for i in 0..(days - 1)
  money = gets.to_i
  left = 0
  right = n - 1

  while left != right do
    middle = (left + right) / 2

    if prices[middle] <= money
      left = middle + 1
    else
      right = middle
    end

  end

  if prices[left] <= money
      puts left + 1
  else
      puts left
  end
end