#include <iostream>

using namespace std;

int main()
{
    int a,b,c;
    cin >> a >> b >> c;
    if((b-a) == 0 || ((c != 0) && ((b-a) % c == 0) && ((b-a) / c > 0)))
        cout << "YES";
    else
        cout << "NO";
    return 0;
}